const express = require('express');
const jwt = require('jsonwebtoken');
var router = express.Router();
var connection = require('../db');
var bodyParser = require('body-parser');
router.use(bodyParser.json());
//var ro = require('../app');
const Joi = require('joi');


// App.js   /signin
router.post('/seller',(req,res)=>
{   
  const data = req.body;
  const schema = Joi.object().keys
  (
      {
        email: Joi.string().email({ minDomainAtoms: 2 }),
        psswrd: Joi.string().required(),
      });
  Joi.validate(data, schema, (err, value) => 
  {
       if (err) 
       {
              res.status(422).json(
                  {
                    status: 'error',
                    message: 'Invalid request data',
                    data: data,
                    error: err
                  });
      } 
      else 
      {
        var email = req.body.email;
        var psswrd = req.body.psswrd;
        var sql = `SELECT * FROM customers WHERE Email=? AND User_Password=?`;
        connection.query(sql,[email,psswrd],function(err,row)
        {
           if(err)
           {
             console.log(err);
             res.status(500).send({err})
           }
           else if(!row.length)
           {
             console.log(err);
             res.status(401).send({"error":"No User Exist!"})
           }else
           {
            if(email == row[0].Email)
           {     
             const token = jwt.sign({ email }, 'my_secret_key');
             res.json(
                 {
                   row,
                   token : token
                 });
             console.log("SignIn Granted!")
            } 
            }
     })
   
}
  })
});


//    Selller Profile Updating

router.put('/updateUser/:id', ensureToken,function(req, res, next) 
{
  const data = req.body;
  // define the validation schema
  const schema = Joi.object().keys
  ({
    f_name : Joi.string(),
    l_name: Joi.string(),
    email: Joi.string().email({ minDomainAtoms: 2 }),
    password : Joi.string(),
  });

  // validate the request data against the schema
  Joi.validate(data, schema, (err, value) => 
  {
    if (err) 
    {
        res.status(422).json(
            {
              message: 'Invalid request data',
              error: err
            });
        console.log("Invalid Request Data : " + err);
    } 
    else 
    {
       jwt.verify(req.token,'my_secret_key',function(err,data){{
       if(err)
       {
          res.status(403).send({ error: 'Access Denied' });
          console.log("Access Denied!" );
      }
      else
      {
        var id = req.params.id;
        var f_name = req.body.f_name;
        var l_name = req.body.l_name;
        var email = req.body.email;
        var password =  req.body.password;
        var sql = `UPDATE customers SET  First_Name="${f_name}",Last_Name="${l_name}",Email="${email}",User_Password = "${password}" WHERE Id=${id}`;
          connection.query(sql, function(err, result) {
          if(err) 
          {
            res.status(500).send({ error: 'Following Error Occured : '+err });
            console.log("Following Error Occured : "+err);
          }
          else
          {
            res.send({text:"Profile Updated!"});
            console.log("Profile Updated!");
          }
   })
   }
  }})
    }
}) 
});


router.post('/buyer',(req,res)=>
{   
   const data = req.body;
   // define the validation schema
   const schema = Joi.object().keys(
       {
         email: Joi.string().email({ minDomainAtoms: 2 }),
         psswrd: Joi.string().required(),
       });
 
       // validate the request data against the schema
       Joi.validate(data, schema, (err, value) => 
       {
         if (err) 
         {
              res.status(422).json(
                  {
                    status: 'error',
                    message: 'Invalid request data',
                    data: data,
                    error : err
                  });
         } 
         else 
         {
           var email = req.body.email;
           var psswrd = req.body.psswrd;
           var sql = `SELECT * FROM customers WHERE Email=? AND User_Password=?`;
           connection.query(sql,[email,psswrd],function(err,row)
           {
              if(err)
            {
             console.log(err);
             res.status(500).send({err})
            }
            else if(!row.length)
            {
             console.log("Authentication Denied");
             res.send({"error":"Authentication Denied"})
            }
            else
            {
              if(email == row[0].Email)
              {     
                const token = jwt.sign({ email }, 'my_secret_key');
                res.json(
                    {
                      row,
                      token : token
                    });
      console.log("SignIn Granted!")
} }
 
    })
} 
})

});



//    Buyer Profile Updating

// router.put('/updateBuyer/:id', ensureToken, function(req, res, next) 
// {
  
//    // fetch the request data
//    const data = req.body;
//   // define the validation schema
//   const schema = Joi.object().keys
//   ({
//     f_name : Joi.string().required(),
//     l_name: Joi.string().required(),
//     email: Joi.string().email({ minDomainAtoms: 2 }),
//     password : Joi.string(),
//   });

//   // validate the request data against the schema
//   Joi.validate(data, schema, (err, value) => 
//   {
//      if (err) 
//      {
//          res.status(422).json(
//            {
//              status: 'error',
//              message: 'Invalid request data',
//              data: data
//            });
//          console.log("Invalid Request Data!");
//      } 
//      else 
//      {
//         jwt.verify(req.token,'my_secret_key',function(err,data)
//         {{
//            if(err)
//             { 
//               res.status(403).send({ error: 'Access Denied' });
//               console.log("Access Denied!");
//             }
//           else
//            {
//               var id = req.params.id;
//               var f_name = req.body.f_name;
//               var l_name = req.body.l_name;
//               var email = req.body.email;
//               var password = req.body.password;
        
//               var sql = `UPDATE customers SET  First_Name="${f_name}",Last_Name="${l_name}",
//               Email="${email}", User_Password = "${password}"  WHERE Id=${id}`;
//               connection.query(sql, function(err, result) 
//               {
//                 if(err) 
//                  {
//                    res.status(500).send({ error: 'Following Error Occured : '+err });
//                    console.log("Following Error Occured : "+err);
//                  }
//                 else
//                  {
//                    res.send({text:"Profile Updated!"});
//                    console.log("Profile Updated!");
//                  }
//               })
//            }
//         }})
//     }
// }) 
// });


//   Admin SignIn


//  App.js   /signin
router.post('/admin',(req,res)=>
{
    var email = req.body.email;
    var psswrd = req.body.psswrd;
    var sql = `SELECT * FROM admins WHERE Admin_Email=?`;
    connection.query(sql,[email],function(err,row)
    {
        if(err)
        {
           console.log(err);
           res.status(500).send({err})
        }
        else if(!row.length)
        {
            console.log("Authentication Denied");
            res.status(401).send({"error":"Authentication Denied"})
            
        }
        else
        {
          console.log(psswrd);
          console.log(row[0].Admin_Password);
          console.log(row[0].Admin_Email);
          console.log(email);
          if(email == row[0].Admin_Email && psswrd == row[0].Admin_Password)
        {     
          const token = jwt.sign({ email }, 'my_secret_key');
          res.json(
            {
              row,
              token : token,
           //   status : "Login SuccessFul"
            });
        }
        else
        {
          res.status(401).send({error:"Access Denied"})
          console.log("Access Denied")
        } }
 
    })
});



function ensureToken(req,res,next)
{
    const bearerHeader = req.headers["authorization"];
    if (typeof bearerHeader !== "undefined")
    {
        const bearer = bearerHeader.split(" ");
        const bearerToken = bearer[1];
        req.token = bearerToken;
        next();
    }
    else 
    {
        res.sendStatus(403);
    }
}

module.exports = router;

